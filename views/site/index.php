<?php

/** @var yii\web\View $this */

$this->title = 'introduccion';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 3 de Yii</h1>

        <p class="lead">Ejemplo de aplicacion en MVC con PHP</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Modelos</h2>

                <p>Utilizacion de modelos para conectarnos con una tabla de la base de datos</p>
            </div>
            <div class="col-lg-4">
                <h2>Menu</h2>

                <p>Gestion del menu de la aplicacion para conectarnos con las distintas acciones de los controladores</p>
                
            </div>
            <div class="col-lg-4">
                <h2>CRUD</h2>

                <p>Creacion de un CRUD con gii y explicar las distintas acciones del controlador y las vistas</p>
                            </div>
        </div>

    </div>
</div>
