<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Entradas;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $datos, // el dataprovider que he creado en el controlador para mandarle a la vista
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], // numero de serie
            // campos de la tabla
            'id',
            'titulo',
            'texto',
            'fecha',
            // fin de los campos de la tabla
        ],
    ]); ?>


</div>
